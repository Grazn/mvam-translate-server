#!/bin/sh
. venv/bin/activate
# flask translate compile
exec gunicorn --workers=1 -b :9999 --access-logfile - --error-logfile - mvam:app
