#!/bin/bash

echo "Updating repository"
git pull
echo "Building new docker image"
sudo docker build -t mvam-translate:latest .

echo "Stopping old image"
sudo docker stop mvam-translate
echo "Removing old container"
sudo docker rm mvam-translate
echo "Running new container"
sudo docker run \
                --name mvam-translate \
                -d \
                -p 9999:9999 \
                --restart always \
                mvam-translate:latest