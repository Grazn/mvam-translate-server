from flask import Blueprint, request, jsonify
from googletrans import Translator

api = Blueprint('api', __name__)


@api.route('/translate', methods=['POST'])
def translate():
    content = request.json

    src_lang = content['source_lang']
    trg_lang = content['target_lang']
    text = content['text']

    translator = Translator(service_urls=[
        'translate.google.com',
        'translate.google.be',
    ])

    print(f'Translating {src_lang} -> {trg_lang}: {text}')
    translation = translator.translate(text=text, src=src_lang, dest=trg_lang)

    response = {
        'text': translation.text
    }

    return jsonify(response)
