from flask import Flask


def init_app(app: Flask):
    from .api import api
    from .main import main

    app.register_blueprint(api)
    app.register_blueprint(main)
