from flask import Flask
from flask_cors import CORS

from config import Config


def create_app():
    import app.routes as routes

    app = Flask(__name__)
    CORS(app, resources={r'/*': {'origins': Config.CORS_URLS}})

    ################
    #    Routes    #
    ################
    routes.init_app(app)

    return app
