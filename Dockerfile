FROM tiangolo/uwsgi-nginx:python3.7

WORKDIR /home/mvam

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app/ app/
#COPY static/ static/
#COPY templates/ templates/
COPY mvam.py boot.sh ./
COPY config.py.prod config.py
RUN chmod +x boot.sh

ENV FLASK_APP app

EXPOSE 9999

ENTRYPOINT ["./boot.sh"]
